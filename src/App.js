import styled from 'styled-components';
import React, { useEffect, useRef } from 'react';
import 'mind-ar';
import './App.css';

const AppStyles = styled.div`
  width: 100vw;
  height: 100vh;
`;

function App() {
  const sceneRef = useRef(null);
  const targetRef = useRef(null);

  // mindAR needs this effect to start in React
  useEffect(() => {
    const sceneEl = sceneRef.current;
    const targetEl = targetRef.current;
    const arSystem = sceneEl.systems['mindar-system'];
    sceneEl.addEventListener('renderstart', () => {
      arSystem.start(); // start mindAR
    });
    // detect target found
    targetEl.addEventListener('targetFound', (event) => {
      console.log('target found');
    });

    // detect target lost
    targetEl.addEventListener('targetLost', (event) => {
      console.log('target lost');
    });
    return () => {
      arSystem.stop();
    };
  }, []);

  return (
    <AppStyles>
      <a-scene
        ref={sceneRef}
        mindar="imageTargetSrc: https://immense-refuge-77873.herokuapp.com/https://home.jonnypage.ca/iversoft/mindar-targets/flowers.mind; autoStart: false; uiLoading: no; uiError: no; uiScanning: no;"
        color-space="sRGB"
        embedded
        renderer="colorManagement: true, physicallyCorrectLights"
        vr-mode-ui="enabled: false"
        device-orientation-permission-ui="enabled: false"
      >
        <a-camera position="0 0 0" look-controls="enabled: false"></a-camera>

        <a-entity mindar-image-target="targetIndex: 0" ref={targetRef}>
          <a-box color="red" scale="0.4 0.4 0.1" position="0 0 0.05"></a-box>
          <a-plane
            color="green"
            position="0 0 0"
            scale="1 0.65 1"
            rotation="0 0 0"
          ></a-plane>
        </a-entity>
      </a-scene>
    </AppStyles>
  );
}

export default App;
